# download and prepare
FROM alpine:latest as source
ENV MODPACK_URL="https://mediafilez.forgecdn.net/files/4497/274/server-1.2.3.zip"
WORKDIR /source
RUN apk upgrade --update-cache --available && \
    apk add wget openssl
RUN wget ${MODPACK_URL} -O server.zip
RUN unzip server.zip
RUN rm server.zip

# run
EXPOSE 25565/tcp
FROM alpine:latest as run
WORKDIR /instance
RUN apk upgrade --update-cache --available && \
    apk add --no-cache openjdk17 openjdk17 bash
COPY --from=source /source/server-1.2.3 /instance
COPY start.sh /instance/start.sh
RUN chmod +x /instance/start.sh
ENTRYPOINT exec /instance/start.sh
VOLUME /instance
